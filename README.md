# Assurity TM

Automation test coverage covering the TradeMe sandbox API for Assurity Consulting.

## Scope

Theses tests currently cover the following areas:
- TradeMe Sandbox Categories #6327

## Prerequisites

These tests require Python 3+. They were written using Python 3.9.7.

It uses the following modules:
- Pytest (https://pypi.org/project/pytest/) `pip install pytest`

## Setup & Run
1. Ensure environment is setup to use Python and Pytest `python -V && pytest --version`
1. Clone this repository `git clone git@gitlab.com:joseph_holland/assurity-tm.git`
1. Execute `pytest` in the root directory

## HTML Output
Results for this test may be generated in an HTML report using a Pytest add-on called pytest-html (https://pypi.org/project/pytest-html/)

Install with pip `pip install pytest-html`

To generate a report, execute `pytest --html=path/to/report.html`
