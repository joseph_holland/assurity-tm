import json, pytest, requests

@pytest.fixture()
def api_data():
    r = requests.get("https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false")
    yield json.loads(r.text)

class TestCategories6327:
    def test_should_have_correct_name(self, api_data):
        assert "Name" in api_data
        assert api_data["Name"] == "Carbon credits"

    def test_should_relist(self, api_data):
        assert "CanRelist" in api_data
        assert api_data["CanRelist"]

    def test_should_describe_gallery_promotion(self, api_data):
        assert "Promotions" in api_data
        
        gallery = next((x for x in api_data["Promotions"] if x["Name"] == "Gallery"), {})
        assert "Description" in gallery

        assert gallery["Description"] == "Good position in category"